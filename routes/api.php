<?php

use Illuminate\Support\Facades\Route;
use Superius\OmniHorizon\Http\Controllers\HorizonController;

Route::group(['middleware' => 'dev_api_token', 'prefix' => 'dev/horizon'], function () {
    Route::post('/whitelist/add-ip-address', [HorizonController::class, 'addIpAddressOnWhitelist']);
    Route::post('/whitelist/delete', [HorizonController::class, 'removeWhitelist']);
});
