<?php

namespace Superius\OmniHorizon\Enums;

enum HorizonFileNameEnum: string
{
    case WHITE_LIST_FILE = 'telescope_ip_whitelist';
}
