<?php

namespace Superius\OmniHorizon\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Superius\OmniHorizon\Enums\HorizonFileNameEnum;
use Superius\OmniHorizon\Requests\CreateHorizonWhitelistRequest;

class HorizonController extends Controller
{
    public function addIpAddressOnWhitelist(CreateHorizonWhitelistRequest $request): JsonResponse
    {
        $ip_address = $request->get('ip_address') ?: Request::ip();

        if (Storage::exists(\App\Enums\HorizonFileNameEnum::WHITE_LIST_FILE->value)) {
            $file = Storage::get(HorizonFileNameEnum::WHITE_LIST_FILE->value);
            if ($file && Str::contains($file, $ip_address)) {
                return response()->json(['message' => 'IP address already exists on the list!'], 409);
            }

            Storage::append(HorizonFileNameEnum::WHITE_LIST_FILE->value, $ip_address);

            return response()->json(['message' => 'IP address successfully appended on the list!']);
        }
        Storage::put(HorizonFileNameEnum::WHITE_LIST_FILE->value, $ip_address);

        return response()->json([
            'message' => 'File ' . HorizonFileNameEnum::WHITE_LIST_FILE->value .
                ' successfully created and IP address ' . $ip_address .
                ' added on the list!',
        ]);
    }

    public function removeWhitelist(): JsonResponse
    {
        if (Storage::exists(HorizonFileNameEnum::WHITE_LIST_FILE->value)) {
            Storage::delete(HorizonFileNameEnum::WHITE_LIST_FILE->value);

            return response()->json(['message' => 'File ' . HorizonFileNameEnum::WHITE_LIST_FILE->value .
                ' successfully deleted!',
            ]);
        }

        return response()->json(['message' => 'File ' . HorizonFileNameEnum::WHITE_LIST_FILE->value .
            ' does not exist!',
        ], 404);
    }
}
