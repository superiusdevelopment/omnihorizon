<?php

namespace Superius\OmniHorizon\Providers;

use Superius\OmniDebugAssistant\Enums\TelescopeFileNameEnum;
use Superius\OmniHorizon\Commands\HorizonListen;
use Superius\OmniHorizon\Enums\HorizonFileNameEnum;
use Illuminate\Queue\Listener;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Laravel\Horizon\HorizonApplicationServiceProvider;
use Laravel\Horizon\SystemProcessCounter;
use Laravel\Horizon\WorkerCommandString;

class HorizonServiceProvider extends HorizonApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // Configure Queue Listener class for HorizonListen command
        $this->app->when(HorizonListen::class)->needs(Listener::class)->give(function ($app) {
            return new Listener(base_path());
        });

        // Configure Horizon to run horizon:listen instead of horizon:work
        WorkerCommandString::$command = 'exec @php artisan horizon:listen';
        SystemProcessCounter::$command = 'horizon:listen';
    }

    /**
     * Register the Horizon gate.
     *
     * This gate determines who can access Horizon in non-local environments.
     *
     * @return void
     */
    protected function gate(): void
    {
        Gate::define('viewHorizon', function ($user = null) {
            $userIp = Request::ip();

            if (Str::startsWith($userIp, config('horizon.allowed_ip_range.us'))) {
                return true;
            }

            if (data_get($_SERVER, 'REMOTE_ADDR') === config('mailapp.debug.remote_address')) {
                return true;
            }

            $whitelistFile = HorizonFileNameEnum::WHITE_LIST_FILE->value;
            if (Storage::exists($whitelistFile) && Storage::get($whitelistFile) && $userIp) {
                return Str::contains(Storage::get($whitelistFile), $userIp);
            }

            return false;
        });
    }
}
