<?php

namespace Superius\OmniHorizon\Providers;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Superius\OmniHorizon\Commands\HorizonListen;

class OmniHorizonServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //scripts that prepares build
        if ($this->app->runningInConsole()) {
            $this->commands([
                HorizonListen::class
            ]);
        }
    }

    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../config/horizon.php' => config_path('horizon.php')
        ]);

        $this->registerRoutes();
    }

    protected function registerRoutes(): void
    {
        Route::group([
            'prefix' => 'api',
        ], function () {
            $this->loadRoutesFrom(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' .
                DIRECTORY_SEPARATOR . 'routes' . DIRECTORY_SEPARATOR . 'api.php');
        });
    }
}
