<?php

namespace Superius\OmniHorizon\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateHorizonWhitelistRequest extends FormRequest
{
    /**
     * @return array<string,array<mixed>>
     */
    public function rules(): array
    {
        return [
            'ip_address' => ['sometimes', 'nullable', 'string', 'ip'],
        ];
    }
}
